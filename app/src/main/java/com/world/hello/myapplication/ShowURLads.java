package com.world.hello.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;


public class ShowURLads extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_propeller_opener);
        loadURL();
        showBrowser();

    }



    protected void showBrowser(){

        //open web browser with any URL from your JSON

        Intent viewIntent = new Intent("android.intent.action.VIEW");
        viewIntent.setData(Uri.parse(loadURL()));
        viewIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(viewIntent);

      finish();


    }
    public String loadURL()
    {

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        String propellerurl = preferences.getString("urlid", "");
        Log.d("propeller",propellerurl);

        return propellerurl;
    }
}
