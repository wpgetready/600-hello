package com.world.hello.myapplication;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;

import com.appnext.ads.interstitial.Interstitial;
import com.appnext.base.Appnext;
import com.appnext.core.callbacks.OnAdClicked;
import com.appnext.core.callbacks.OnAdClosed;
import com.appnext.core.callbacks.OnAdError;
import com.appnext.core.callbacks.OnAdLoaded;
import com.appnext.core.callbacks.OnAdOpened;

public class ShowAppnext extends Activity {
    Interstitial interstitial_Ad;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_appnext);

        //we need to init appnext first
        Appnext.init(this);

        //declare intersitial ads class object and load appnextID
       interstitial_Ad = new Interstitial(this, idAppnext());
       interstitial_Ad.loadAd();
// Get callback for ad loaded
        interstitial_Ad.setOnAdLoadedCallback(new OnAdLoaded() {
            @Override
            public void adLoaded(String s) {
                interstitial_Ad.showAd();
            }

        });// Get callback for ad opened
        interstitial_Ad.setOnAdOpenedCallback(new OnAdOpened() {
            @Override
            public void adOpened() {

            }
        });// Get callback for ad clicked
        interstitial_Ad.setOnAdClickedCallback(new OnAdClicked() {
            @Override
            public void adClicked() {

            }
        });// Get callback for ad closed
        interstitial_Ad.setOnAdClosedCallback(new OnAdClosed() {
            @Override
            public void onAdClosed() {
                ShowAppnext.this.finish();

            }
        });// Get callback for ad error
        interstitial_Ad.setOnAdErrorCallback(new OnAdError() {
            @Override
            public void adError(String error) {
               ShowAppnext.this.finish();
            }
        });
    }


    @Override
    public void onRestart() {
        super.onRestart();
        ShowAppnext.this.finish();
    }
    public String idAppnext()
    {

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        String appnextid = preferences.getString("idAppnext", "82096619-fa22-4027-8fe7-f6c3a925c98f");

        return appnextid;
    }
}
