package com.world.hello.myapplication;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.preference.PreferenceManager;
import android.util.Log;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;


public class ShowAdmob extends Activity {
    String admobid, admobplacement;
    //variables to assign values from shared preferences

    private InterstitialAd mInterstitialAd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_show_mob);
        admobid();
//we are getting admob id and placement id, assigning to new variables

        //admob ID initialize
        MobileAds.initialize(this, admobid);
        //we need to wait, 10 seconds to initialize admob
        new CountDownTimer(10000, 1000) {

            public void onTick(long millisUntilFinished) {

            }


            public void onFinish() {
                Log.i("FZSM","Admob initialized.");
initad();
            }
            //after 10 seconds, we can init our application

        }.start();
    }
    @Override
    public void onRestart() {
        super.onRestart();
        ShowAdmob.this.finish();
    }


      public void initad()
      {

          mInterstitialAd = new InterstitialAd(this);
          mInterstitialAd.setAdUnitId(admobplacement); //we assign admob placement ID
          mInterstitialAd.loadAd(new AdRequest.Builder().addTestDevice("6DCCCA4DB54B3472EC12018E02B187CB").build());
          mInterstitialAd.setAdListener(new AdListener() {
              @Override
              public void onAdLoaded() {
                  mInterstitialAd.show();
              }

              @Override
              public void onAdFailedToLoad(int errorCode) {
                  ShowAdmob.this.finish();

              }

              @Override
              public void onAdOpened() {
                  // Code to be executed when the ad is displayed.
              }

              @Override
              public void onAdLeftApplication() {

              }

              @Override
              public void onAdClosed() {
                  ShowAdmob.this.finish();

              }
          });
      }


    public void admobid()
    {
        Log.i("FZSM","Admob preferences loadaed");
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        admobid = preferences.getString("idAdmob", "ca-app-pub-3940256099942544/1033173712");
        admobplacement = preferences.getString("placementAdmob", "ca-app-pub-3940256099942544/1033173712");

    }

    }
