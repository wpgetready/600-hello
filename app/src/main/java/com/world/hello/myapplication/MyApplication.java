package com.world.hello.myapplication;

import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.os.Build;
import android.util.Log;

import com.evernote.android.job.Job;
import com.evernote.android.job.JobConfig;
import com.evernote.android.job.JobManager;

public class MyApplication extends Application {
    public static final String CHANNEL_ID = "channelNotification";
    private static Context mContext;
    @Override
    public void onCreate() {
        super.onCreate();

        JobManager.create(this).addJobCreator(new JobHelperSingleton());
        //This is only for debugging and Android M (Android 6) Api 23
        //https://en.wikipedia.org/wiki/Android_version_history
        //https://developer.android.com/reference/android/os/Build.VERSION_CODES


        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            JobConfig.setAllowSmallerIntervalsForMarshmallow(true);
            Log.i("FZSM", "Marshmallow detected. Smaller intervals in action!");
        } else
        {
            Log.i("FZSM", "Warning: no Marshmallow detected. Smaller intervals not allowed!");
        }
        createNotificationChannel();

    }
    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel serviceChannel = new NotificationChannel(
                    CHANNEL_ID,
                    "Example Service Channel",
                    NotificationManager.IMPORTANCE_DEFAULT
            );
            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(serviceChannel);
        }
    }
    public static Context getContext(){
        return mContext;
}}