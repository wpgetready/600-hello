package com.world.hello.myapplication;

public class Helper {}

/*This class DOES NOTHING. It only provides info for different Android versions
The following information has been combined from
https://en.wikipedia.org/wiki/Android_version_history and
https://developer.android.com/reference/android/os/Build.VERSION_CODES

because there is no a common place combinining all data.

V.N. : Version number
LKV: Linux Version Number

Code name           V.N.        LKV     Date Release        API         VERSION_CODES
(No codename) 	    1.0 	    ? 	    September 23, 2008 	1           BASE
Petit Four 	        1.1 	    2.6 	February 9, 2009 	2           BASE_1_1
Cupcake 	        1.5 	    2.6.27 	April 27, 2009 	    3           CUPCAKE
                                                                        CUR_DEVELOPMENT (Not an official release)
Donut 	            1.6 	    2.6.29 	September 15, 2009 	4           DONUT
Eclair 	            2.0 – 2.1 	2.6.29 	October 26, 2009 	5 – 7       ECLAIR,ECLAIR_0_1,ECLAIR_MR1 (posible return values depending on date)
Froyo 	            2.2 – 2.2.3 	2.6.32 	May 20, 2010 	    8           FROYO
Gingerbread 	    2.3 – 2.3.7 	2.6.35 	December 6, 2010 	9 – 10      GINGERBREAD,GINGERBREAD_MR1
Honeycomb 	        3.0 – 3.2.6 	2.6.36 	February 22, 2011 	11 – 13     HONEYCOMB,HONEYCOMB_MR1,HONEYCOMB_MR2
Ice Cream Sandwich 	4.0 – 4.0.4 	3.0.1 	October 18, 2011 	14 – 15     ICE_CREAM_SANDWICH,ICE_CREAM_SANDWICH_MR1
Jelly Bean 	        4.1/4.3.1 	*(1)	July 9, 2012 	    16 – 18     JELLY_BEAN,JELLY_BEAN_MR1,JELLY_BEAN_MR2
KitKat 	            4.4 – 4.4.4 	3.10 	October 31, 2013 	19 – 20     KITKAT,KITKAT_WATCH
Lollipop 	        5.0 – 5.1.1 	3.16 	November 12, 2014 	21 – 22     LOLLIPOP,LOLLIPOP_MR1
Marshmallow 	    6.0 – 6.0.1 	3.18 	October 5, 2015 	23          M
Nougat 	            7.0 – 7.1.2 	4.4 	August 22, 2016 	24 – 25     N,N_MR1
Oreo 	            8.0 – 8.1 	4.10 	August 21, 2017 	26 – 27     O
Pie 	            9.0 	    *(2) 	August 6, 2018 	    28          P
Android Q 	        10.0 			                        29          Q

*(1): 3.0.31/3.4.39
*(2): 4.4.107/4.9.84/4.14.42


 */
