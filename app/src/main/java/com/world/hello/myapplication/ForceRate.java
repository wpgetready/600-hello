package com.world.hello.myapplication;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;

public class ForceRate extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_force);


        //first we are checking if user clicked OK button, if yes, don't show again
        String loaded;
        loaded = loadPreferences();
        if(loaded.equals("0"))
        {
            //show dialog if user never clicked OK button
            showRateDialog();
        }
    }


    //method to load preferences
    public String loadPreferences()
    {

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        String load = preferences.getString("load", "0");
        return load;
    }

    //method to save preferences
    public void savePreferences() {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("load", "1");
        editor.commit();

    }


    //method to show rate dialog
    public void showRateDialog()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("title of your dialog")
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        savePreferences();

                        //redirect to Play Store
                        final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                        try {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                        } catch (android.content.ActivityNotFoundException anfe) {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                        }
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }
}
