package com.world.hello.myapplication;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;


public class RestartDevice extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        JobStarter.SchedulePeriodicJob();
    }
}