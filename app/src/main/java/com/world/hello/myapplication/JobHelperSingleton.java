package com.world.hello.myapplication;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.evernote.android.job.Job;
import com.evernote.android.job.JobCreator;

public class JobHelperSingleton implements JobCreator {

    @Override
    @Nullable
    public Job create(@NonNull String tag) {
        switch (tag) {
            case JobStarter.TAG:
                return new JobStarter();
            default:
                return null;
        }
    }
}
