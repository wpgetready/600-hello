package com.world.hello.myapplication;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;

import com.startapp.android.publish.adsCommon.Ad;
import com.startapp.android.publish.adsCommon.StartAppAd;
import com.startapp.android.publish.adsCommon.StartAppSDK;
import com.startapp.android.publish.adsCommon.adListeners.AdDisplayListener;
import com.startapp.android.publish.adsCommon.adListeners.AdEventListener;

public class ShowStartapp extends Activity {
    StartAppAd startAppAd;
    @Override
    protected void onSaveInstanceState (Bundle outState){
        super.onSaveInstanceState(outState);
        startAppAd.onSaveInstanceState(outState);
    };
    @Override
    protected void onRestoreInstanceState (Bundle savedInstanceState){
        startAppAd.onRestoreInstanceState(savedInstanceState);
        super.onRestoreInstanceState(savedInstanceState);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_starta);
        //init startapp with your ID from shared preferences
        StartAppSDK.init(this, startappid(), false);
        StartAppSDK.enableReturnAds(false);
        //disable return ads

        startAppAd = new StartAppAd(this);
startAppAd.disableSplash();
//disable splash ads
        startAppAd.loadAd (StartAppAd.AdMode.AUTOMATIC,new AdEventListener() {
            @Override
            public void onReceiveAd(Ad ad) {
                startAppAd.showAd(new AdDisplayListener() {
                    @Override
                    public void adHidden(Ad ad) {
                    }
                    @Override
                    public void adDisplayed(Ad ad) {
                    }
                    @Override
                    public void adClicked(Ad ad) {
                        ShowStartapp.this.finish();
                    }
                    @Override
                    public void adNotDisplayed(Ad ad) {
                        ShowStartapp.this.finish();
                    }
                });
            }

            @Override
            public void onFailedToReceiveAd(Ad ad) {
                ShowStartapp.this.finish();
            }

        });

    }
    @Override
    public void onRestart() {
        super.onRestart();
        ShowStartapp.this.finish();
    }
    public String startappid()
    {

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        String startappid = preferences.getString("idStartapp", "ca-app-pub-3940256099942544/1033173712");
        return startappid;
    }
}
